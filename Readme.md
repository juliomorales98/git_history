# Git History

**Git history** is an interface for various git log commands.

Given a git dir and a file, the program can:

* Search history of commits where the file has changed.

* Search for a specific string and show commits where the text appears

* View diff file of selected commit after any of above search

## Screenshots

![Alt text](1.png "")

![Alt text](2.png "")

![Alt text](3.png "")

### Compilation with nuitka

`nuitka main.py --onefile --enable-plugin=pyside6,tk-inter --include-data-files=form.ui=form.ui`
