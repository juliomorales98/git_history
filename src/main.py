# This Python file uses the following encoding: utf-8
import os
from pathlib import Path
import sys
import subprocess

from PySide6.QtWidgets import QApplication, QWidget, QPushButton, QLineEdit, QListView
from PySide6.QtCore import QFile
from PySide6.QtUiTools import QUiLoader
from PySide6.QtGui import QStandardItemModel, QStandardItem

def doCommand(cmd):
    result = subprocess.run(cmd, stdout=subprocess.PIPE, text=True, shell=True)
    output = result.stdout
    return output.split('\n')

class Widget(QWidget):
    def __init__(self):
        super(Widget, self).__init__()
        self.load_ui()
        self.setup_connections()

    def load_ui(self):
        loader = QUiLoader()
        path = os.fspath(Path(__file__).resolve().parent / "form.ui")
        ui_file = QFile(path)
        ui_file.open(QFile.ReadOnly)
        loader.load(ui_file, self)
        ui_file.close()

    def setup_connections(self):
        # Connect the clicked signal of the searchPushButton to the on_search_clicked slot
        search_button = self.findChild(QPushButton, "searchPushButton")
        if search_button:
            search_button.clicked.connect(self.on_search_clicked)

        folder_search_button = self.findChild(QPushButton, "folderPushButton")
        if folder_search_button:
            folder_search_button.clicked.connect(self.on_folder_search_clicked)

        commits_list_view = self.findChild(QListView, "commitsListView")
        if commits_list_view:
            commits_list_view.clicked.connect(self.on_commit_clicked)

        search_text_push_button = self.findChild(QPushButton, "searchTextPushButton")
        if search_text_push_button:
            search_text_push_button.clicked.connect(self.on_text_search_clicked)

    def on_search_clicked(self):
        # This function will be called when the searchPushButton is clicked
        pathLineEdit = self.findChild(QLineEdit, "pathLineEdit")
        fileLineEdit = self.findChild(QLineEdit, "fileLineEdit")

        cmdBase = "git --git-dir=" + pathLineEdit.text() + "/.git --work-tree=" + fileLineEdit.text()
        cmd = cmdBase + " log --oneline --follow -- " + fileLineEdit.text()
        rows = doCommand(cmd)
        commitsListView = self.findChild(QListView, "commitsListView")
        list_model = QStandardItemModel()

        # Set the model for the list view
        commitsListView.setModel(list_model)
        for _ in rows:
            if len(_) > 0:
                item = QStandardItem(_)
                list_model.appendRow(item)

    def on_folder_search_clicked(self):
        import tkinter as tk
        from tkinter import filedialog

        pathLineEdit = self.findChild(QLineEdit, "pathLineEdit")
        pathLineEdit.setText(filedialog.askdirectory())

    def on_commit_clicked(self,index):
        commitsListView = self.findChild(QListView, "commitsListView")
        commit = (index.data().split(" "))[0]
        pathLineEdit = self.findChild(QLineEdit, "pathLineEdit")
        fileLineEdit = self.findChild(QLineEdit, "fileLineEdit")
        cmdBase = "git --git-dir=" + pathLineEdit.text() + "/.git --work-tree=" + fileLineEdit.text()
        cmd = cmdBase + " show "  + commit + " -- " + fileLineEdit.text()
        rows = doCommand(cmd)
        commitsListView = self.findChild(QListView, "diffListView")
        list_model = QStandardItemModel()

        # Set the model for the list view
        commitsListView.setModel(list_model)
        for _ in rows:
            if len(_) > 0:
                item = QStandardItem(_)
                list_model.appendRow(item)
    def on_text_search_clicked(self):
        # This function will be called when the searchPushButton is clicked
        pathLineEdit = self.findChild(QLineEdit, "pathLineEdit")
        fileLineEdit = self.findChild(QLineEdit, "fileLineEdit")
        search_line_edit = self.findChild(QLineEdit, "searchLineEdit")

        cmdBase = "git --git-dir=" + pathLineEdit.text() + "/.git --work-tree=" + fileLineEdit.text()
        cmd = cmdBase + ' log --oneline -i -S"'  + search_line_edit.text() + '" -- ' + fileLineEdit.text()
        rows = doCommand(cmd)
        commitsListView = self.findChild(QListView, "commitsListView")
        list_model = QStandardItemModel()

        # Set the model for the list view
        commitsListView.setModel(list_model)
        for _ in rows:
            if len(_) > 0:
                item = QStandardItem(_)
                list_model.appendRow(item)

if __name__ == "__main__":
    app = QApplication([])
    widget = Widget()
    widget.show()
    print("widget")

    sys.exit(app.exec_())
